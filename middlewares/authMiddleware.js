const jwt = require("jsonwebtoken")


const verifyUserToken = (req, res, next) => {
    const token = req.headers['authorization'];
    if (!token) {
        res.status(401);
        return res.json("Unauthorised");
    }
    jwt.verify(token, "secret-key", (error, data) => {
        if (error) {
            res.status(401);
            return res.send("Invalid token");
        } else {
            req.userDetails = data;
            console.log(data)
            next()
        }
    })
}

const adminMiddleWare = (req,res,next) =>{
    const token = req.headers['authorization'];
    if (!token) {
        res.status(401);
        return res.json("Unauthorised");
    }
    jwt.verify(token, "secret-key", (error, data) => {
        if (error) {
            res.status(401);
            return res.send("Invalid token");
        } else {
            if(data.role === "ADMIN" || data.role === "APPROVAL"){
                req.userDetails = data;
                return next()
            }
            return res.status(401).send("Unauthorised for this api")
        }
    })
}

const approvalMiddleWare = (req,res,next) =>{
    const token = req.headers['authorization'];
    if (!token) {
        res.status(401);
        return res.json("Unauthorised");
    }
    jwt.verify(token, "secret-key", (error, data) => {
        if (error) {
            res.status(401);
            return res.send("Invalid token");
        } else {
            if(data.role === "APPROVAL"){
                req.userDetails = data;
                return next()
            }
            return res.status(401).send("Unauthorised for this api")
        }
    })
}

module.exports = {
    verifyUserToken,
    adminMiddleWare,
    approvalMiddleWare
}
