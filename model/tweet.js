module.exports = class Tweet {
    constructor(obj) {
        this.tweet = obj.tweet;
        this.username = obj.userId;
        this.createdDate = obj.createdDate;
        this.modifiedDate = obj.modifiedDate;
        this.isDeleted = obj.isDeleted;
        this.tweetAudit = obj.tweetAudit;
    }
}


