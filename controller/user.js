const mongoose = require("mongoose");
require("../entity/user")
const userModel = mongoose.model("Users");

const registerUser = (data)=>{
    return new userModel(data).save()
}

const loginUser = (data)=>{
    return userModel.findOne({username : data.username , password : data.password})
}

module.exports = {
    registerUser,
    loginUser
}
