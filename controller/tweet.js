const mongoose = require('mongoose');
require("../entity/tweet")
require("../entity/tweetAudit")
const tweetModel = mongoose.model('Tweets');
const tweetAuditModel = mongoose.model("TweetAudits");


const postTweet = (data) => {
    return new tweetModel(data).save()
}

const getAllActiveTweetByUser = (userName) => {
    return tweetModel.find({username: userName, isDeleted: false});
}

const deleteTweetByUser = (username, tweetId) => {
    return tweetModel.findOneAndUpdate(
        {username: username, _id: tweetId, isDeleted: false},
        {
            $set: {
                isDeleted: true
            }
        }
    )
}

const editTweetAdminAction = async (adminUsername, tweetId, data) => {
    const tweetExistence = await checkTweetExistence(tweetId)
    if(!tweetExistence){
        return Promise.reject({
            "error" : "No such tweet exists"
        })
    }
    const _data = await tweetAuditModel.findOne(
        {
            tweetId: tweetId,
            approvalStatus: "APPROVAL_PENDING"
        }
    )
    if (_data) {
        return Promise.reject({
            "error": "One changes approval is still pending for this tweet"
        })
    }

    return new tweetAuditModel({
        tweetDetails: tweetId,
        adminUserName: adminUsername,
        adminActionType: "EDIT",
        editedValue: data.editedValue,
        adminComment: data.adminComment,
        approvalStatus: "APPROVAL_PENDING"
    }).save()

}

const deleteTweetAdminAction = async (adminUsername , tweetId , data)=>{
    const tweetExistence = await checkTweetExistence(tweetId)
    if(!tweetExistence){
        return Promise.reject({
            "error" : "No such tweet exists"
        })
    }
    const _data = await tweetAuditModel.findOne(
        {
            tweetId: tweetId,
            approvalStatus: "APPROVAL_PENDING"
        }
    )
    if (_data) {
        return Promise.reject({
            "error": "One changes approval is still pending for this tweet"
        })
    }
    return new tweetAuditModel({
        tweetDetails: tweetId,
        adminUserName: adminUsername,
        adminActionType: "DELETE",
        editedValue: null,
        adminComment: data.adminComment,
        approvalStatus: "APPROVAL_PENDING"
    }).save()
}


const findTweetByApprovalStatus = async (approvalStatus) => {
    return tweetAuditModel.find({approvalStatus : approvalStatus}).populate("tweetDetails");
}

const checkTweetExistence = async (tweetId)=>{
    return tweetModel.findOne({_id : tweetId , isDeleted : false}).then((_data)=>{
        if(!_data){
            return false;
        }
        return true
    })
}

const approveAdminActionByApproval = async (tweetAuditId , approvalUsername  ,data) => {
    /**
     * Change the approval status to APPROVED,
     * then in corresponding tweetId change the value of tweet and old tweet
     */
    const today = new Date();
    let _data  = await tweetAuditModel.findOne({_id : tweetAuditId}).populate("tweetDetails")
    if(!_data){
        await Promise.reject({
            "error" : "audit id not found"
        })
    }
    const tweetId = _data["tweetDetails"]["_id"];
    const session = await mongoose.startSession();
    if(_data["adminActionType"] === "EDIT"){
        return session.withTransaction(()=>{
            return Promise.all([
                tweetAuditModel.findOneAndUpdate({_id : tweetAuditId} , {
                    oldTweet : _data["tweetDetails"]["tweet"],
                    approvalUsername : approvalUsername,
                    approvalStatus : "APPROVED",
                    approvalComment : data.approvalComment,
                    approvalActionDate : today
                }),
                tweetModel.findOneAndUpdate({_id : tweetId , isDeleted : false}, {
                    tweet : _data["editedValue"],
                    modifiedDate : today
                })
            ])
        })
    }else if(_data["adminActionType"] === "DELETE"){
        return session.withTransaction(()=>{
            return Promise.all([
                tweetAuditModel.findOneAndUpdate({_id : tweetAuditId} , {
                    oldTweet : _data["tweetDetails"]["tweet"],
                    approvalUsername : approvalUsername,
                    approvalStatus : "APPROVED",
                    approvalComment : data.approvalComment,
                    approvalActionDate : today
                }),
                tweetModel.findOneAndUpdate({_id : tweetId , isDeleted : false}, {
                    isDeleted : true,
                    modifiedDate : today
                })
            ])
        })
    }
}

const findAuditLogByTweet = (tweetId) =>{
    return tweetAuditModel.find({
        "tweetDetails" : tweetId
    }).populate("tweetDetails")
}

module.exports = {
    postTweet,
    getAllActiveTweetByUser,
    deleteTweetByUser,
    editTweetAdminAction,
    findTweetByApprovalStatus,
    approveAdminActionByApproval,
    deleteTweetAdminAction,
    findAuditLogByTweet
}
