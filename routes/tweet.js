const express = require('express');
const router = express.Router();
const tweetController = require("../controller/tweet")
const Tweet = require("../model/tweet")
const {adminMiddleWare} = require("../middlewares/authMiddleware");
const {approvalMiddleWare} = require("../middlewares/authMiddleware");

router.post("/",(req,res)=>{
    let data = {
        tweet : req.body.tweet,
        username : req.userDetails["username"]
    }
    tweetController.postTweet(data).then((_data)=>{
        res.json(new Tweet(_data));
    }).catch((error)=>{
        res.status(500);
        res.send(error);
    })
})

router.get("/",(req,res,next)=>{
    tweetController.getAllActiveTweetByUser(req.userDetails.username).then((_data)=>{
        res.json(_data);
    }).catch((error)=>{
        res.status(500).send(error);
    })
})

router.delete("/:tweetId",(req,res,next)=>{
    tweetController.deleteTweetByUser(req.userDetails.username , req.params.tweetId).then((data)=>{
        res.send(data)
    }).catch((error)=>{
        res.status(500).send(error);
    })
})


router.put("/admin-action/:tweetId",adminMiddleWare , (req,res,next)=>{
    tweetController.editTweetAdminAction(req["userDetails"].username , req.params["tweetId"], {
        editedValue : req.body.editedValue,
        adminComment : req.body.adminComment
    }).then((data)=>{
        res.json(data);
    }).catch((error)=>{
        console.log(error)
        res.status(500).send(error);
    })
})

router.delete("/admin-action/:tweetId" , adminMiddleWare, (req,res,next)=>{
    tweetController.deleteTweetAdminAction(req["userDetails"].username , req.params["tweetId"], {
        adminComment : req.body.adminComment
    }).then((data)=>{
        res.json(data);
    }).catch((error)=>{
        console.log(error)
        res.status(500).send(error);
    })
})


router.get("/approval-status/:approvalStatus", approvalMiddleWare, (req,res,next)=>{
    tweetController.findTweetByApprovalStatus(req.params.approvalStatus).then((_data)=>{
        res.json(_data)
    }).catch((error)=>{
        console.log(error);
        res.status(500).send(error);
    })
})

router.post("/approval-action/:tweetAuditId" , approvalMiddleWare,(req,res,next)=>{
    tweetController.approveAdminActionByApproval(req.params["tweetAuditId"] , req["userDetails"].username , req.body).then((_data)=>{
        res.json(_data);
    }).catch((error)=>{
        console.log(error);
        res.status(500).send(error);
    })
})

router.get("/audit-log/tweetId/:tweetId" , approvalMiddleWare,(req,res,next)=>{
    tweetController.findAuditLogByTweet(req.params.tweetId).then((_data)=>{
        res.json(_data);
    }).catch((error)=>{
        console.log(error);
        res.status(500).send(error);
    })
})



module.exports = router;

