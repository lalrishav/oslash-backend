const express = require('express');
const router = express.Router();
const jwt = require("jsonwebtoken")
const userController = require("../controller/user")




router.post("/register",(req,res,next)=>{
    const username = req.body.username;
    const password = req.body.password;
    const role = req.body.role;
    userController.registerUser({
        username:username,
        password:password,
        role:role
    }).then((data)=>{
        res.send(data);
    }).catch((error)=>{
        res.send(error);
    })
})

router.post("/login" , (req,res,next)=>{
    const username = req.body.username;
    const password = req.body.password;
    userController.loginUser({
        username:username,
        password:password
    }).then((data)=>{
        if(data){
            delete data["password"];
            const token = jwt.sign(JSON.parse(JSON.stringify(data)), 'secret-key');
            res.json({
                token : token
            })
        }else{
            res.status(401);
            res.json({
                "message":"Wrong username password"
            })
        }
    }).catch((error)=>{
        console.log(error)
        res.status(error.statusCode || 500);
        res.send(error);
    })
})



module.exports = router;

