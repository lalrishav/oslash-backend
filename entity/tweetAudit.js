const mongoose = require('mongoose');
require("./tweet")
const Schema = mongoose.Schema;

const TweetAudit = new Schema({
    tweetDetails : {type : Schema.Types.ObjectId , ref : 'Tweets'},
    adminActionType : {type: String},
    adminUserName: {type: String},
    editedValue: {type: String},
    oldTweet : {type:String},
    adminComment: {type: String},
    adminActionDate : {type:Date , default:new Date()},
    approvalUsername : {type:String},
    approvalStatus: {type: String},
    approvalComment: {type: String},
    approvalActionDate : {type:Date},
})

mongoose.model("TweetAudits",TweetAudit, "tweetAudit");
