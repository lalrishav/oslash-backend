const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const tweetSchema = new Schema({
    tweet: {type: String},
    createdDate: {type: Date , default : new Date()},
    modifiedDate: {type: Date , default : new Date()},
    username: {type: String },
    isDeleted: {type: Boolean , default : false}
})


mongoose.model("Tweets",tweetSchema, "tweets");

