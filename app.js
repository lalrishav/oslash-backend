const express = require("express");
const app = express();
const authRoute =  require("./routes/auth");
const tweetRoute = require("./routes/tweet")
const mongoose = require("mongoose");
const jwt = require("jsonwebtoken");
const {verifyUserToken} = require("./middlewares/authMiddleware");

let bodyParser = require("body-parser");

const port = 3000;

app.use(bodyParser.json());
app.use("/auth",authRoute );
app.use(verifyUserToken)

app.use("/tweet", tweetRoute)

app.listen(3000,(error)=>{
    if(error){
        console.log(error);
    }else{
        mongoose.connect("mongodb+srv://oslash:Password210698@cluster0.1askx.mongodb.net/oslash?retryWrites=true&w=majority").then(()=>{
            console.log("server started on port " , port)
        }).catch((error)=>{
            console.log("Error connecting database",error)
        })
    }
})

